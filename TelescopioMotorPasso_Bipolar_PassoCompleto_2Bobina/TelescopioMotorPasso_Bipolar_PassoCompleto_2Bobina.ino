// Controle de Motor de Passo Bipolar com PASSO COMPLETO E 2 BOBINAS.
// Testamos com EM-235 de Impressora 12V (eu acho que era 12V !!)
// o Bipolar EM-235 de Impressora com 12V  gasta 400mA  --> Gira bem pior e mais lento que em MEIO PASSO (80mA)





#define LED_BUILTIN 2
#include <ArduinoOTA.h>   // Library to allow programing via WIFI (Choose ip port in Arduino IDE
//#include <WiFi.h>        // Include the Wi-Fi library
//#include <WebServer.h>
#include <analogWrite.h>  // Aparently, you can analogRead, but not analogWrite without a library with ESP32 !!!

#define m1PWM 23
#define m2PWM 22

// Atenção: Monster Moto Shield ligada em 6v ou mais
#define INA1 21   //Motor1, entrada A
#define INA2 19   //Motor2, entrada A
#define INB1 18   //Motor1, entrada B
#define INB2 5    //Motor2, entrada B


int b1, b2;
int dir1 = 0;
int dir2 = 0;
int speed1 = 255;
int speed2 = 255; 
/*
const char *ssid = "ESP32 Access Point"; // The name of the Wi-Fi network that will be created
const char *password = "your-password";   // The password required to connect to it, leave blank for an open

WebServer server(80);

void handleRoot() {
  server.send(200, "text/plain", "XUPA FEDERAL!");
}

void handleNotFound() {
  server.send(404, "text/plain", "Banana");
}
*/
void setup() {  
  //LED
  pinMode(LED_BUILTIN, OUTPUT);

  //PWM
  pinMode(m1PWM, OUTPUT);     // Initialize the Motor PWM pin as an output
  pinMode(m2PWM, OUTPUT);
  pinMode(INA1, OUTPUT);
  pinMode(INB1, OUTPUT);
  pinMode(INA2, OUTPUT);
  pinMode(INB2, OUTPUT);
  
  //analogWriteFrequency(200);  

  digitalWrite(INA1, 0); // Frente
  digitalWrite(INB1, 1);
  digitalWrite(INA2, 0);  // Frente
  digitalWrite(INB2, 1);

  digitalWrite(m1PWM, 1);  // Sepeed = 0
  digitalWrite(m2PWM, 1);

  // Serial Begin
  Serial.begin(115200);
  Serial.println("Booting");

/*
  // WIFI Server AP (This Create a WIFI Server in ESP32 with IP: 192.168.4.1  by default) 
  WiFi.softAP(ssid, password);             // Start the access point
  Serial.print("Access Point \"");
  Serial.print(ssid);
  Serial.println("\" started");

  Serial.print("IP address:\t");
  Serial.println(WiFi.softAPIP());         // Send the IP address of the ESP32 to the computer

  server.on("/", handleRoot);  
    
  server.onNotFound(handleNotFound);

  server.on("/ligaled", []() {
    server.send(200, "text/plain", "ligou");
    digitalWrite(LED_BUILTIN, HIGH);
  });

  server.on("/desligaled", []() {
    server.send(200, "text/plain", "apagou");
    digitalWrite(LED_BUILTIN, LOW);
  });

  server.begin();
  Serial.println("HTTP server started");
  //END OF WIFI Server AP


  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { 
      type = "filesystem";
    }

    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
  Serial.println("Ready");

  // END OF OTA SETUP
  */
}

void loop() {
//  ArduinoOTA.handle();
//  server.handleClient();

      for(int i = 0; i < 4; i++) {
        switch(i) {
          case 0:
              b1=2;
              b2=2;
          break;
          case 1:
              b1=1;
              b2=2;
          break;
          case 2:
              b1=1;
              b2=1;
          break;
          case 3:
              b1=2;
              b2=1;
          break;
        }

        switch(b1) {
          case 0:
              digitalWrite(INA1, 0);
              digitalWrite(INB1, 0);
          break;
          case 1:
              digitalWrite(INA1, 1);
              digitalWrite(INB1, 0);
          break;
          case 2:
              digitalWrite(INA1, 0);
              digitalWrite(INB1, 1);
          break;
        }
      
        switch(b2) {
          case 0:
              digitalWrite(INA2, 0);
              digitalWrite(INB2, 0);
          break;
          case 1:
              digitalWrite(INA2, 1);
              digitalWrite(INB2, 0);
          break;
          case 2:
              digitalWrite(INA2, 0);
              digitalWrite(INB2, 1);
          break;
        }
        
        delay(18);  // Max 9 - 7V  ou 15 - 12V
    }
   
   //  Serial.println("----------------------");
}
